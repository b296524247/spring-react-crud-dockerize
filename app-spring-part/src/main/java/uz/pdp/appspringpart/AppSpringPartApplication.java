package uz.pdp.appspringpart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppSpringPartApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSpringPartApplication.class, args);
    }

}
