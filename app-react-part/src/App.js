import axios from "axios";
import {useEffect, useState} from "react";

const App = () => {
    const [books,setBooks] = useState([]);
    const getBooks = ()=>{
        axios.get('http://localhost:8787/book')
            .then(res=>{
                setBooks(res.data._embedded.books)
            })
    }

    useEffect(() => {
        getBooks()
    }, []);
    return (
        <>
            <h1>
                Hello world
            </h1>
            <ul>
                {books.map((book,i)=>
                <li>{i+'-'+book.title}</li>
                )}
            </ul>
        </>
    )
        ;
}

export default App;
